# Running reg-app in a Docker environment

A docker image is provided to run a demo version of the reg-app. It can be used as a base to configure and run the reg-app, depending on your needs. **Attention:** The docker configartion files are examples. You habe to edit them to reflect your local setup.

```sh
mkdir regapp-docker
cd regapp-docker
wget 'https://git.scc.kit.edu/reg-app/regapp-docker/-/raw/main/docker-compose.yml'
docker-compose up
```

This docker configuration exposes TCP Port 8080 and 9990. On Port 9990 you can reach the wildfly administration console. It is protected with standard credentials. Please change these credentials:

```sh
docker ps
docker exec -it <container-id-from-docker-ps> /bin/bash
/opt/jboss/wildfly/bin/add-user.sh -u admin -p <thenewpassword>
```

Next, change the reg-app admin password. Visit `http://localhost:8080/admin/user/list-admin-users.xhtml` and choose user 'admin' (login with credentials 'admin' and 'secret'). Click '+' on the 'Password' panel and set a new password. Choose a hash algorithm if you like.

You should have a running environment now to explore and test. If you want to further expose the reg-app, it is recommended to do so via a reverse proxy.

### Using swag

In order to use [swag](https://github.com/linuxserver/docker-swag) as reverse proxy, you have to edit and use the docker-compose-swag.yml file. You have to edit at least these lines:
```yaml
      - TZ=Europe/Berlin
      - URL=<url> # for example host.example.com
      - SUBDOMAINS=<subdomains if any> # can be empty or a comma separated list
```

Please see the [swag](https://github.com/linuxserver/docker-swag) homepage for all options and possibilities. Start your container using the right docker-compose file:

```sh
docker-compose -f docker-compose-swag.yml up
```


