Build your own docker image:
docker build --network=host -t <image-name> . --build-arg CHECKOUT_NAME=origin/main --build-arg CHECKOUT_BRANCH_NAME=local_main

Replace <image-name> with your desired name for the image.

