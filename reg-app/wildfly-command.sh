#!/bin/bash

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh

echo "==> Setup Wildfly..."
$JBOSS_CLI << EOF
embed-server --server-config=standalone-full-ha.xml --std-out=echo

batch

module add --name=org.postgresql --resources=/tmp/postgresql-42.2.4.jar --dependencies=javax.api,javax.transaction.api

/subsystem=datasources/jdbc-driver=postgresql:add(driver-name=postgresql,driver-module-name=org.postgresql,driver-xa-datasource-class-name=org.postgresql.xa.PGXADataSource)

data-source add --jndi-name=$JNDI_NAME --name=$DS_NAME --connection-url=jdbc:postgresql://\${env.DB_HOST}:\${env.DB_PORT}/\${env.DB_NAME} --driver-name=postgresql --user-name=\${env.DB_USER} --password=\${env.DB_PASS}

/subsystem=datasources/data-source=ds\/bwidmDS:write-attribute(name=background-validation, value=true)
/subsystem=datasources/data-source=ds\/bwidmDS:write-attribute(name=valid-connection-checker-class-name, value=org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker)
/subsystem=datasources/data-source=ds\/bwidmDS:write-attribute(name=statistics-enabled, value=true)

/subsystem=mail/mail-session=regapp-mail:add(jndi-name=java:/mail/bwIdmMail)
/socket-binding-group=standard-sockets/remote-destination-outbound-socket-binding=regapp-smtp-binding:add(host=\${env.MAIL_HOST:localhost},port=\${env.MAIL_PORT:25})
/subsystem=mail/mail-session=regapp-mail/server=smtp:add(outbound-socket-binding-ref=regapp-smtp-binding)

jms-queue add --queue-address=bwIdmMailQueue --entries=["queue/bwIdmMailQueue","java:jboss/exported/jms/queue/bwIdmMailQueue"]
jms-queue add --queue-address=bwIdmAsyncJobQueue --entries=["queue/bwIdmAsyncJobQueue","java:jboss/exported/jms/queue/bwIdmAsyncJobQueue"]

/subsystem=logging/logger=org.hibernate.engine.internal.StatisticalLoggingSessionEventListener:add(level=WARN)

/subsystem=undertow/server=default-server/host=default-host/location=\/:remove()
/subsystem=undertow/configuration=handler/file=welcome-content:remove()

/subsystem=undertow/server=default-server/https-listener=https:write-attribute(name=proxy-address-forwarding, value=\${env.PROXY_ADD_FWD:false})

/subsystem=transactions:write-attribute(name=default-timeout,value=1800)

run-batch

batch

/subsystem=jgroups/stack=tcp-kub:add()
/subsystem=jgroups/stack=tcp-kub/transport=TCP:add(socket-binding="jgroups-tcp")
/subsystem=jgroups/stack=tcp-kub/protocol=dns.DNS_PING:add(add-index=0)
/subsystem=jgroups/stack=tcp-kub/protocol=dns.DNS_PING/property=dns_query:add(value=\${env.DNS_PING_SERVICE_NAME:kubernetes.default.local})
/subsystem=jgroups/stack=tcp-kub/protocol=dns.DNS_PING/property=async_discovery_use_separate_thread_per_request:add(value=true)
/subsystem=jgroups/stack=tcp-kub/protocol=MERGE3:add()
/subsystem=jgroups/stack=tcp-kub/protocol=FD_SOCK:add()
/subsystem=jgroups/stack=tcp-kub/protocol=FD_ALL:add()
/subsystem=jgroups/stack=tcp-kub/protocol=VERIFY_SUSPECT:add()
/subsystem=jgroups/stack=tcp-kub/protocol=pbcast.NAKACK2:add()
/subsystem=jgroups/stack=tcp-kub/protocol=UNICAST3:add()
/subsystem=jgroups/stack=tcp-kub/protocol=pbcast.STABLE:add()
/subsystem=jgroups/stack=tcp-kub/protocol=pbcast.GMS:add()
/subsystem=jgroups/stack=tcp-kub/protocol=MFC:add()
/subsystem=jgroups/stack=tcp-kub/protocol=FRAG2:add()

/subsystem=jgroups/channel=ee:write-attribute(name=stack,value=tcp-kub)

/subsystem=messaging-activemq/server=default/cluster-connection=my-cluster:write-attribute(name=max-retry-interval, value=60000L)
/subsystem=messaging-activemq/server=default/cluster-connection=my-cluster:write-attribute(name=retry-interval, value=5000L)
/subsystem=messaging-activemq/server=default/cluster-connection=my-cluster:write-attribute(name=retry-interval-multiplier, value=2)

run-batch

stop-embedded-server

EOF

