# How to use this to setup an postgres for local development

- the `init_regapp_dev.sh` loads the `regapp_dev.sql` with psql 
- the `init_regapp_dev.sh` changes the config `max_prepared_transactions`

## change database name, ...

you can change the Datebase name, username, password inside the compose.yml

## create file `regapp_dev.sql`

Wichtig: Ohne privilegien `-O` = no owner `-x` = no privileges

~~~
pg_dump -h localhost -U regapp_dev -O -x regapp_dev > regapp_dev.sql
~~~

add the file aside the init_regapp_dev.sh file.

## Start docker-compose

~~~
> cd docker-db-dev
> docker-compose.exe -f compose.yml up
~~~

um eine neue/fische datenbank anzulegen, einfach das `data_pg_regapp_dev` Verzeichnis löschen.



