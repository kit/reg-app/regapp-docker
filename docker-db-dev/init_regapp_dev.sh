#!/bin/bash
set -e

# Wait for PostgreSQL to start
until pg_isready -U "$POSTGRES_USER" -d "$POSTGRES_DB"; do
  echo "Waiting for PostgreSQL to start..."
  sleep 2
done

# Load the dump file
psql -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f /var/lib/postgresql/regapp_dev.sql

# --- max_prepared_transactions ---
# Pfad zur postgresql.conf
PG_CONF="/var/lib/postgresql/data/postgresql.conf"

# Überprüfen, ob die Datei existiert
if [ -f "$PG_CONF" ]; then
    # max_prepared_transactions auf 32 setzen
    sed -i "s/^#max_prepared_transactions = .*/max_prepared_transactions = 32/" "$PG_CONF"
    echo "max_prepared_transactions wurde auf 32 gesetzt."
else
    echo "postgresql.conf nicht gefunden."
    exit 1
fi

# PostgreSQL neu starten
pg_ctl restart
